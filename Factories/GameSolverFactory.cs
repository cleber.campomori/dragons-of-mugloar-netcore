using System.Collections.Generic;
using DragonsOfMugloar.NetCore.Entities;
using DragonsOfMugloar.NetCore.Services.GameSolver.Impl;
using DragonsOfMugloar.NetCore.Services.GameSolver.Interfaces;

namespace DragonsOfMugloar.NetCore.Factories
{
    public class GameSolverFactory
    {
        private static Dictionary<Weather, IGameSolver> _solverByWeatherDictionary = new Dictionary<Weather, IGameSolver>()
        {
            { Weather.Flood, new FloodGameSolver()},
            { Weather.Fog, new FogGameSolver()},
            { Weather.LongDry, new LongDryGameSolver()},
            { Weather.NormalWeather, new NormalWeatherGameSolver()},
            { Weather.Storm, new StormGameSolver()},
        };

        public static IGameSolver GetGameSolverByWeather(Weather weather)
        {
            if (_solverByWeatherDictionary.ContainsKey(weather))
            {
                return _solverByWeatherDictionary[weather];
            }
            // The implementation for the weather does not exist. 
            // TODO: Throw exception!
            return null;
        }
    }
}