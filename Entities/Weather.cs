using System.ComponentModel;
using DragonsOfMugloar.NetCore.Attributes;

namespace DragonsOfMugloar.NetCore.Entities
{
    public enum Weather
    {
        [Description("Normal Weather")]
        [EnumValue("NMR")]
        NormalWeather,

        [Description("Storm")]
        [EnumValue("SRO")]
        Storm,

        [Description("Heavy Rain with Floods")]
        [EnumValue("HVA")]
        Flood,

        [Description("The Long Dry")]
        [EnumValue("T E")]
        LongDry,

        [Description("Fog")]
        [EnumValue("FUNDEFINEDG")]
        Fog
    }
}