namespace DragonsOfMugloar.NetCore.Entities
{
    public class Game
    {
        public long GameId { get; set; }
        public Knight Knight { get; set; }
        public Dragon Dragon { get; set; }
        public Weather Weather { get; set; }

        public Game()
        {
            Knight = new Knight();
            Dragon = new Dragon();
        }

        public override string ToString()
        {
            string result = string.Empty;
            result = $"Game: [GameId: {this.GameId}";
            if (this.Knight != null)
            {
                result += $", Knight: {this.Knight.ToString()}";
            }
            if (this.Dragon != null)
            {
                result += $", Dragon: {this.Dragon.ToString()}";
            }
            result += $", Weather: {this.Weather.ToString()}]";
            return result;
        }
    }
}