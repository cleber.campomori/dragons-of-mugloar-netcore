namespace DragonsOfMugloar.NetCore.Entities
{
    public class SolveResult
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}
