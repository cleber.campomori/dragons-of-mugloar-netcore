namespace DragonsOfMugloar.NetCore.Entities
{
    public class Dragon
    {
        public int ScaleThickness { get; set; }
        public int ClawSharpness { get; set; }
        public int WingStrength { get; set; }
        public int FireBreath { get; set; }

        public Dragon()
        {
            // Default constructor to keep "anonymous" constructor (constructor initializing properties)
        }

        public Dragon(int scaleThickness, int clawSharpness, int wingStrength, int fireBreath)
        {
            this.ScaleThickness = scaleThickness;
            this.ClawSharpness = clawSharpness;
            this.WingStrength = wingStrength;
            this.FireBreath = fireBreath;
        }

        public override string ToString() => $"Dragon: [ScaleThickness = {this.ScaleThickness}, ClawSharpness = {this.ClawSharpness}, WingStrength = {this.WingStrength}, FireBreath = {this.FireBreath}]";
    }
}