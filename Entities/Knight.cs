namespace DragonsOfMugloar.NetCore.Entities
{
    public class Knight
    {
        public string Name { get; set; }
        public int Attack { get; set; }
        public int Armor { get; set; }
        public int Agility { get; set; }
        public int Endurance { get; set; }

        public override string ToString() => $"Knight: [Name = {this.Name}, Attack = {this.Attack}, Armor = {this.Armor}, Agility = {this.Agility}, Endurance = {this.Endurance}]";
    }
}