using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DragonsOfMugloar.NetCore.Attributes;
using DragonsOfMugloar.NetCore.Entities;

namespace DragonsOfMugloar.NetCore.Extensions
{
    public static class EnumValueExtensions
    {
        public static Weather GetWeatherFromCode(this string weatherValue)
        {
            List<Weather> weathers = Enum.GetValues(typeof(Weather)).Cast<Weather>().ToList();
            return weathers.SingleOrDefault(w => w.GetWeatherCode() == weatherValue);
        }

        public static string GetWeatherCode(this Weather weather)
        {
            string name = Enum.GetName(typeof(Weather), weather);
            EnumValueAttribute enumValue = typeof(Weather).GetField(name)
                        .GetCustomAttributes(false)
                        .OfType<EnumValueAttribute>()
                        .SingleOrDefault();
            if (enumValue == null)
            {
                // TODO: throw exception!
                return null;
            }
            return enumValue.Value;
        }

        public static string GetWeatherDescription(this Weather weather)
        {
            string name = Enum.GetName(typeof(Weather), weather);
            DescriptionAttribute description = typeof(Weather).GetField(name)
                        .GetCustomAttributes(false)
                        .OfType<DescriptionAttribute>()
                        .SingleOrDefault();
            if (description == null)
            {
                // TODO: throw exception!
                return weather.ToString();
            }
            return description.Description;
        }
    }
}