namespace DragonsOfMugloar.NetCore.Specs
{
    public class RunStatistics
    {
        public int Runs { get; private set; }
        public int Wins { get; private set; } = 0;
        public int Defeats { get; private set; } = 0;
        public int StormDefeats { get; private set; } = 0;

        private decimal _winPercentage = 0;
        public decimal WinPercentage
        {
            get
            {
                return ((decimal)Wins / (decimal)Runs) * 100;
            }
            private set
            {
                _winPercentage = value;
            }
        }

        public string WinPercentageClassification
        {
            get
            {
                return this.WinPercentage >= 60 ? "GOOD" : "BAD";
            }
        }

        public RunStatistics(int runs)
        {
            Runs = runs;
        }

        public void UpdateStatistics(BattleResult battleResult)
        {
            if (battleResult.DragonWasWinner)
            {
                Wins++;
            }
            else
            {
                if (battleResult.Weather == Entities.Weather.Storm)
                {
                    StormDefeats++;
                }
                Defeats++;
            }
        }
    }
}