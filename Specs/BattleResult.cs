using DragonsOfMugloar.NetCore.Entities;

namespace DragonsOfMugloar.NetCore.Specs
{
    public class BattleResult
    {
        public bool DragonWasWinner { get; private set; } = true;
        public string Message { get; private set; } = string.Empty;
        public Weather Weather { get; private set; }

        public BattleResult(bool isWinner, string message, Weather weather)
        {
            this.DragonWasWinner = isWinner;
            this.Message = message;
            this.Weather = weather;
        }

    }
}