using System;

namespace DragonsOfMugloar.NetCore.Attributes
{
    [AttributeUsage(validOn: AttributeTargets.Field)]
    public class EnumValueAttribute : Attribute
    {
        public string Value { get; private set; }

        public EnumValueAttribute(string value)
        {
            Value = value;
        }
    }
}