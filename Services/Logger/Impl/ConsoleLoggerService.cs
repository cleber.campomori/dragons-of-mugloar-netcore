using System;
using DragonsOfMugloar.NetCore.Services.Logger.Base;

namespace DragonsOfMugloar.NetCore.Services.Logger.Impl
{
    public class ConsoleLoggerService : ILoggerService
    {
        public void Debug(string message)
        {
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine($"[DEBUG][{DateTime.Now.ToString()}]  >>> {message}");
            Console.ResetColor();
        }

        public void Error(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"[ERROR][{DateTime.Now.ToString()}]  !!! {message} !!!");
            Console.ResetColor();
        }

        public void Info(string message)
        {
            Console.WriteLine($"[INFO ][{DateTime.Now.ToString()}] {message}");
        }
    }
}