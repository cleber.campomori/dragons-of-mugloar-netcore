namespace DragonsOfMugloar.NetCore.Services.Logger.Base
{
    public interface ILoggerService
    {
        void Debug(string message);
        void Info(string message);
        void Error(string message);
    }
}
