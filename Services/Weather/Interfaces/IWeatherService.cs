namespace DragonsOfMugloar.NetCore.Services.Weather.Interfaces
{
    public interface IWeatherService
    {
        Entities.Weather GetWeatherForGameId(long gameId);
    }
}