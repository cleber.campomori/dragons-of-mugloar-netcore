using System.Net.Http;
using System.Xml;
using System.Xml.XPath;
using DragonsOfMugloar.NetCore.Extensions;
using DragonsOfMugloar.NetCore.Services.Logger.Base;
using DragonsOfMugloar.NetCore.Services.Weather.Interfaces;

namespace DragonsOfMugloar.NetCore.Services.Weather.Impl
{
    public class RoyalWeatherService : IWeatherService
    {
        private HttpClient _client = new HttpClient();
        private ILoggerService _loggerService;

        public RoyalWeatherService(ILoggerService loggerService)
        {
            _loggerService = loggerService;
        }

        public Entities.Weather GetWeatherForGameId(long gameId)
        {
            string weatherRoute = $"http://www.dragonsofmugloar.com/weather/api/report/{gameId}";
            _loggerService.Debug($"Calling weather service: {weatherRoute}");
            HttpResponseMessage weatherResponse = _client.GetAsync(weatherRoute).Result;
            string weatherXml = weatherResponse.Content.ReadAsStringAsync().Result;
            XmlDocument xmlDocument = new XmlDocument();
            _loggerService.Debug($"Preparing document to fetch weather code...");
            xmlDocument.LoadXml(weatherXml);
            XPathNavigator xmlNavigator = xmlDocument.CreateNavigator();
            _loggerService.Debug($"Applying xPath expression (/report/code/text())...");
            XPathNavigator weatherCodeNode = xmlNavigator.SelectSingleNode("/report/code/text()");
            _loggerService.Debug($"Weather Code fetched: {weatherCodeNode.Value}");
            return weatherCodeNode.Value.GetWeatherFromCode();
        }
    }
}