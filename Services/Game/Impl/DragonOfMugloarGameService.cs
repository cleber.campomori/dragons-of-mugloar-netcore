using System.Net.Http;
using System.Text;
using DragonsOfMugloar.NetCore.Entities;
using DragonsOfMugloar.NetCore.Extensions;
using DragonsOfMugloar.NetCore.Factories;
using DragonsOfMugloar.NetCore.Services.Game.Base;
using DragonsOfMugloar.NetCore.Services.GameSolver.Interfaces;
using DragonsOfMugloar.NetCore.Services.Logger.Base;
using DragonsOfMugloar.NetCore.Services.Weather.Interfaces;
using DragonsOfMugloar.NetCore.Specs;
using Newtonsoft.Json;

namespace DragonsOfMugloar.NetCore.Services.Game.Impl
{
    public sealed class DragonOfMugloarGameService : GameServiceBase
    {
        private HttpClient _client = new HttpClient();

        public DragonOfMugloarGameService(IWeatherService weatherService, ILoggerService loggerService)
            : base(weatherService, loggerService)
        { }

        protected override Entities.Game DoStartGame()
        {
            _loggerService.Debug($"Fetching a new game from http://www.dragonsofmugloar.com/api/game...");
            HttpResponseMessage response = _client.GetAsync("http://www.dragonsofmugloar.com/api/game").Result;
            Entities.Game game = new Entities.Game();
            if (response.IsSuccessStatusCode)
            {
                _loggerService.Debug("Start battle endpoint returned a success status code.");
                _loggerService.Debug($"Success! Reading the game from API response...");
                game = response.Content.ReadAsAsync<Entities.Game>().Result;
            }
            return game;
        }

        public override BattleResult SolveGame(Entities.Game game)
        {
            IGameSolver gameSolver = GameSolverFactory.GetGameSolverByWeather(game.Weather);
            _loggerService.Debug($"Using strategy {gameSolver.GetType().FullName} to create a dragon for the weather {game.Weather.GetWeatherCode()}...");
            game.Dragon = gameSolver.SolveGameForKnight(game.Knight);
            _loggerService.Info($"Generated dragon: {game.Dragon.ToString()}");
            string solveUrl = $"http://www.dragonsofmugloar.com/api/game/{game.GameId}/solution";
            _loggerService.Debug($"Sending dragon to battle using {solveUrl}...");
            _loggerService.Info("Sending generated dragon to the battle...");
            SolveAnswer answer = new SolveAnswer();
            answer.Dragon = game.Dragon;
            //
            string json = JsonConvert.SerializeObject(answer);
            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = _client.PutAsync(solveUrl, content).Result;
            //
            if (response.IsSuccessStatusCode)
            {
                _loggerService.Debug("Solve endpoint returned a success status code.");
                _loggerService.Debug($"Success! Reading the result from API response...");
                SolveResult solveResult = response.Content.ReadAsAsync<SolveResult>().Result;
                return new BattleResult(isWinner: solveResult.Status == "Victory", message: solveResult.Message, weather: game.Weather);
            }
            // TODO: throw exception: we can't solve battle
            return null;
        }
    }
}