using DragonsOfMugloar.NetCore.Services.Logger.Base;
using DragonsOfMugloar.NetCore.Services.Weather.Interfaces;
using DragonsOfMugloar.NetCore.Specs;

namespace DragonsOfMugloar.NetCore.Services.Game.Base
{
    public abstract class GameServiceBase
    {
        private IWeatherService _weatherService;
        protected ILoggerService _loggerService;

        public GameServiceBase(IWeatherService weatherService, ILoggerService loggerService)
        {
            _weatherService = weatherService;
            _loggerService = loggerService;
        }

        public Entities.Game StartGame()
        {
            Entities.Game game = DoStartGame();
            game.Weather = _weatherService.GetWeatherForGameId(game.GameId);
            _loggerService.Debug($"Fetched game: {game.ToString()}");
            return game;
        }
        protected abstract Entities.Game DoStartGame();
        public abstract BattleResult SolveGame(Entities.Game game);
    }
}