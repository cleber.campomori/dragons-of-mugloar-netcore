using System;
using System.Collections.Generic;
using System.Linq;
using DragonsOfMugloar.NetCore.Entities;
using DragonsOfMugloar.NetCore.Services.GameSolver.Interfaces;

namespace DragonsOfMugloar.NetCore.Services.GameSolver.Impl
{
    public class NormalWeatherGameSolver : IGameSolver
    {
        // TODO: move to a factory
        // *** Solver Map
        private Dictionary<string, Func<Knight, Dragon>> _solverMethods = new Dictionary<string, Func<Knight, Dragon>>
        {
            {"[Attack]", (knight) =>
                {
                    if (knight.Agility == 0)
                    {
                        return new Dragon(knight.Attack + 2, knight.Armor - 1, 0, knight.Endurance - 1);
                    }
                    return new Dragon(knight.Attack + 2, knight.Armor, knight.Agility - 1, knight.Endurance - 1);
                }
            },
            {"[Armor]", (knight) =>
                {
                    if (knight.Agility == 0)
                    {
                        return new Dragon(knight.Attack - 2, knight.Armor + 2, 0, knight.Endurance);
                    }
                    return new Dragon(knight.Attack, knight.Armor + 2, knight.Agility - 1, knight.Endurance - 1);
                }
            },
            {"[Agility]", (knight) =>
                {
                    if (knight.Endurance - 1 < 0)
                    {
                        return new Dragon(knight.Attack - 1, knight.Armor - 1, knight.Agility + 2, knight.Endurance) ;
                    }
                    else if (knight.Armor - 1 < 0)
                    {
                        //return new Dragon(knight.Attack - 1, 0, knight.Agility - 1, knight.Endurance + 2) ;
                        return new Dragon(knight.Attack, 0, knight.Agility + 2, knight.Endurance - 2) ;
                    }
                    return new Dragon(knight.Attack, knight.Armor - 1, knight.Agility + 2, knight.Endurance - 1) ;
                }
            },
            {"[Endurance]", (knight) =>
                {
                    if (knight.Armor -1 < 0)
                    {
                        return new Dragon(knight.Attack - 1, 0, knight.Agility - 1, knight.Endurance + 2);
                    }
                    else if (knight.Agility -1 < 0)
                    {
                        //return new Dragon(knight.Attack, knight.Armor - 1, knight.Agility - 1, knight.Endurance + 2);
                        return new Dragon(knight.Attack - 1, knight.Armor - 1, 0, knight.Endurance + 2);
                    }
                    return new Dragon(knight.Attack, knight.Armor - 1, knight.Agility - 1, knight.Endurance + 2);
                }
            },
            {"[Agility][Endurance]", (knight) =>
                {
                    if (knight.Armor - 1 < 0)
                    {
                        return new Dragon(knight.Attack - 1, 0, knight.Agility - 1, knight.Endurance + 2);
                    }
                    return new Dragon(knight.Attack, knight.Armor - 1, knight.Agility - 1, knight.Endurance + 2);
                }
            },
            {"[Armor][Agility]", (knight) =>
                {
                    if (knight.Attack - 1 < 0)
                    {
                        return new Dragon(0, knight.Armor + 2, knight.Agility - 1, knight.Endurance - 1);
                    }
                    return new Dragon(knight.Attack - 1, knight.Armor + 2, knight.Agility - 2, knight.Endurance + 1);
                }
            },
            {"[Attack][Endurance]", (knight) =>
                {
                    if (knight.Agility == 0)
                    {
                        return new Dragon(knight.Attack + 2, knight.Armor - 2, 0, knight.Endurance);
                    }
                    return new Dragon(knight.Attack + 2, knight.Armor, knight.Agility - 1, knight.Endurance - 1);
                }
            },
            {"[Attack][Armor]", (knight) => new Dragon(knight.Attack + 2, knight.Armor - 2, knight.Agility, knight.Endurance) },
            {"[Attack][Agility]", (knight) =>
                {
                    if (knight.Armor - 1 < 0)
                    {
                        return new Dragon(knight.Attack + 2, 0, knight.Agility - 1, knight.Endurance - 1);
                    }
                    return new Dragon(knight.Attack + 2, knight.Armor - 1, knight.Agility - 1, knight.Endurance);
                }
            },
            {"[Armor][Endurance]", (knight) =>
                {
                    if (knight.Agility - 1 < 0)
                    {
                        return new Dragon(knight.Attack - 1, knight.Armor - 1, 0, knight.Endurance + 2);
                    }
                    return new Dragon(knight.Attack, knight.Armor - 1, knight.Agility - 1, knight.Endurance + 2);
                }
            },
            {"[Attack][Armor][Agility]", (knight) => new Dragon(knight.Attack + 2, knight.Armor, knight.Agility - 2, knight.Endurance) },
            {"[Attack][Agility][Endurance]", (knight) => new Dragon(knight.Attack + 2, knight.Armor, knight.Agility - 2, knight.Endurance) },
            {"[Armor][Agility][Endurance]", (knight) => new Dragon(knight.Attack, knight.Armor + 2, knight.Agility, knight.Endurance - 2) },
            {"[Attack][Armor][Agility][Endurance]", (knight) => new Dragon(7, 4, 4, 5) }
        };
        //

        public Dragon SolveGameForKnight(Knight knight)
        {
            string solverMethodKey = this.GetBetterAttributesFromKnight(knight);
            if (_solverMethods.ContainsKey(solverMethodKey))
            {
                Func<Knight, Dragon> solverMethod = _solverMethods[solverMethodKey];
                return solverMethod(knight);
            }
            // TODO: solver method not implemented... Throw exception!
            return new Dragon();
        }

        private string GetBetterAttributesFromKnight(Knight knight)
        {
            string result = string.Empty;
            int biggestAttrValue = 0;
            Type knightType = knight.GetType();
            knightType.GetProperties().ToList().ForEach((pi) =>
            {
                if (pi.PropertyType == typeof(int))
                {
                    string propertyName = pi.Name;
                    int propertyValue = (int)knightType.GetProperty(pi.Name).GetValue(knight);
                    if (propertyValue > biggestAttrValue)
                    {
                        biggestAttrValue = propertyValue;
                        result = $"[{propertyName}]";
                    }
                    else if (propertyValue == biggestAttrValue)
                    {
                        result += $"[{propertyName}]";
                    }
                }
            });
            return result;
        }
    }
}