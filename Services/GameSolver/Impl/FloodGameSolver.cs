using DragonsOfMugloar.NetCore.Entities;
using DragonsOfMugloar.NetCore.Services.GameSolver.Interfaces;

namespace DragonsOfMugloar.NetCore.Services.GameSolver.Impl
{
    public class FloodGameSolver : IGameSolver
    {
        public Dragon SolveGameForKnight(Knight knight)
        {
            Dragon result = new Dragon();
            result.FireBreath = 0;
            int armor = knight.Armor;
            result.ClawSharpness = knight.Endurance;
            result.ScaleThickness = knight.Attack;
            result.WingStrength = knight.Agility;
            while (result.ClawSharpness < 10)
            {
                if (armor > 0)
                {
                    result.ClawSharpness += 1;
                    armor--;
                }
                else
                {
                    if (result.ScaleThickness > 0)
                    {
                        result.ClawSharpness += 1;
                        result.ScaleThickness -= 1;
                    }
                    else if (result.WingStrength > 0)
                    {
                        result.ClawSharpness += 1;
                        result.WingStrength -= 1;
                    }
                    if (result.ClawSharpness == 0 && result.WingStrength == 0)
                    {
                        break;
                    }
                }
            }
            if (armor > 0)
            {
                if (armor % 2 == 0)
                {
                    result.ScaleThickness += armor / 2;
                    result.WingStrength += armor / 2;
                }
                else
                {
                    result.ScaleThickness++;
                    armor--;
                    result.ScaleThickness += armor / 2;
                    result.WingStrength += armor / 2;
                }
            }
            return result;
        }
    }
}