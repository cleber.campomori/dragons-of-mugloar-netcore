
using DragonsOfMugloar.NetCore.Entities;
using DragonsOfMugloar.NetCore.Services.GameSolver.Interfaces;

namespace DragonsOfMugloar.NetCore.Services.GameSolver.Impl
{
    public class LongDryGameSolver : IGameSolver
    {
        public Dragon SolveGameForKnight(Knight knight) =>
            // We need a ZEN dragon
            new Dragon
            {
                ScaleThickness = 5,
                ClawSharpness = 5,
                WingStrength = 5,
                FireBreath = 5
            };
    }
}