using DragonsOfMugloar.NetCore.Entities;
using DragonsOfMugloar.NetCore.Services.GameSolver.Interfaces;

namespace DragonsOfMugloar.NetCore.Services.GameSolver.Impl
{
    public class FogGameSolver : IGameSolver
    {
        public Dragon SolveGameForKnight(Knight knight) =>
            // Dragons ALWAYS WIN!
            new Dragon
            {
                ScaleThickness = knight.Attack,
                ClawSharpness = knight.Armor,
                WingStrength = knight.Agility,
                FireBreath = knight.Endurance
            };
    }
}