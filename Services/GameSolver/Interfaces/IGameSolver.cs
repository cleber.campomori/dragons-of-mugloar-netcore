namespace DragonsOfMugloar.NetCore.Services.GameSolver.Interfaces
{
    public interface IGameSolver
    {
        Entities.Dragon SolveGameForKnight(Entities.Knight knight);
    }
}