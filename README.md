# Dragons of Mugloar - .NET Core Version

This is a .NET Core version created to solve the "Dragons of Mugloar" challenge.

### Author

Cleber Lopes Campomori (<cleber.campomori@gmail.com>)

### Languages, frameworks, libraries and tools

  - C#;
  - .NET Core 2.1;
  - Visual Studio Code.

### How to run this solution?

  1) Install the .NET Core 2.1.x SDK. You can download the SDK [here](https://www.microsoft.com/net/download/);
  2) Clone this repo;
  3) Navigate to the folder where the cloned code is using Terminal;
  4) Run `dotnet restore` inside the folder where the code is. This command is necessary only for the first time. With this command, .NET CLI will download and install all the necessary dependencies automatically;
  5) Run `dotnet run` to run the solution.

### A bug (or feature?) found in one of the endpoints...

During my tests, I found a "strange" behavior in the endpoint `http://www.dragonsofmugloar.com/api/game/{gameid}/solution` [`PUT`]. If the weather for the battle is "Storm" and I send the data in the right format... 

```json
{
    "dragon": {
        "scaleThickness": 4,
        "clawSharpness": 5,
        "wingStrength": 3,
        "fireBreath": 8
    }
}
```

... I receive the following answer:

```json
{
    "status": "Defeat",
    "message": "Dragon died in the storm."
}
```

I think this is correct. In the documentation, we can notice that no dragon can battle during a storm.
But, if I send the data in this other format...

```json
{
    "scaleThickness": 4,
    "clawSharpness": 5,
    "wingStrength": 3,
    "fireBreath": 8
}
```

... I receive the following response...

```json
{
    "status": "Victory",
    "message": "Knight died in storm, dragon survived in the pen."
}
```

... which is wrong by the documentation.

I don't know if this is really a bug or a feature. Anyway, the data is always sent in the right format in this solution. ;)