﻿using DragonsOfMugloar.NetCore.Services.Game.Impl;
using DragonsOfMugloar.NetCore.Services.Game.Base;
using DragonsOfMugloar.NetCore.Services.Weather.Impl;
using Microsoft.Extensions.DependencyInjection;
using DragonsOfMugloar.NetCore.Services.Logger.Base;
using DragonsOfMugloar.NetCore.Services.Logger.Impl;
using DragonsOfMugloar.NetCore.Services.Weather.Interfaces;
using DragonsOfMugloar.NetCore.Extensions;
using DragonsOfMugloar.NetCore.Specs;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;

namespace DragonsOfMugloar.NetCore.Interface
{
    class Program
    {
        private static ServiceProvider _diContainer;
        private static GameServiceBase _gameService;
        private static ILoggerService _loggerService;

        static void Main(string[] args)
        {
            #region Configuration Methods
            ConfigureDiContainer();
            ConfigureServices();
            #endregion
            _loggerService.Info("******************************************************");
            _loggerService.Info("*     WELCOME TO DRAGONS OF MUGLOAR GAME SOLVER!     *");
            _loggerService.Info("******************************************************");
            _loggerService.Info("How many games do you want to run? -> ");
            try
            {
                int runs = Convert.ToInt32(Console.ReadLine());
                RunStatistics statistics = new RunStatistics(runs);
                for (int i = 1; i <= statistics.Runs; i++)
                {
                    _loggerService.Info("=========================================");
                    _loggerService.Info($"Running game {i} of {statistics.Runs}...");
                    _loggerService.Info("Fetching a new game from APIs...");
                    Entities.Game game = _gameService.StartGame();
                    _loggerService.Info("A NEW BATTLE WILL START!");
                    _loggerService.Info($" * Weather Information: [{game.Weather.GetWeatherCode()}] {game.Weather.GetWeatherDescription()}");
                    _loggerService.Info($" * The dragon will fight against the knight {game.Knight.Name.ToUpper()}");
                    _loggerService.Info($"      |- Attack: {game.Knight.Attack}");
                    _loggerService.Info($"      |- Armor: {game.Knight.Armor}");
                    _loggerService.Info($"      |- Agility: {game.Knight.Agility}");
                    _loggerService.Info($"      |- Endurance: {game.Knight.Endurance}");
                    _loggerService.Info($"Trying to create a dragon to solve the game {game.GameId}...");
                    BattleResult battleResult = _gameService.SolveGame(game);
                    statistics.UpdateStatistics(battleResult);
                    if (battleResult.DragonWasWinner)
                    {
                        _loggerService.Info($" WINNER \\o/ The dragon won the battle against the knight {game.Knight.Name}!!!");
                        _loggerService.Info($"      |- {battleResult.Message}");
                    }
                    else
                    {
                        _loggerService.Info($" DEFEAT :( The dragon was defeated by the knight {game.Knight.Name}!!!");
                        _loggerService.Info($"      |- {battleResult.Message}");
                    }
                }
                _loggerService.Info("*******************************************************");
                _loggerService.Info("*                  BATTLE STATISTICS                  *");
                _loggerService.Info("*******************************************************");
                _loggerService.Info($" - Battles: {statistics.Runs}");
                _loggerService.Info($" - Number of wins: {statistics.Wins}");
                _loggerService.Info($" - Number of defeats (total): {statistics.Defeats}");
                _loggerService.Info($" - Number of defeats (storm): {statistics.StormDefeats}");
                _loggerService.Info($" - Win Percentage: {statistics.WinPercentage}% [{statistics.WinPercentageClassification }]");
                _loggerService.Info("*******************************************************");
                _loggerService.Info("Bye! ;)");
            }
            catch (Exception ex)
            {
                _loggerService.Error($"We cannot prepare dragons for the battle due a error - {ex.GetType().FullName} : {ex.Message}");
                System.Environment.Exit(-1);

            }
        }

        private static void ConfigureServices()
        {
            _loggerService = _diContainer.GetService<ILoggerService>();
            _gameService = _diContainer.GetService<GameServiceBase>();
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
        }

        private static void ConfigureDiContainer()
        {
            _diContainer = new ServiceCollection()
                // Logger
                .AddSingleton<ILoggerService, ConsoleLoggerService>()
                // Weather
                .AddSingleton<IWeatherService, RoyalWeatherService>()
                // Game
                .AddSingleton<GameServiceBase, DragonOfMugloarGameService>()
                .BuildServiceProvider();

        }
    }
}
